package com.cybage.valueObject;

import com.cybage.entity.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplateVO {
	
	private  User user;
	private Department department;

}

//public static void copyProperties(Object source, Object target)
//try to use this method to combine user and department objects in service
