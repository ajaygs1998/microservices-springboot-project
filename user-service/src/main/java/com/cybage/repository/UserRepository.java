package com.cybage.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cybage.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserId(Long userId);

}
