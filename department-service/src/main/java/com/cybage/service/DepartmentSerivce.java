package com.cybage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybage.entity.Department;
import com.cybage.repository.DepartmentRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DepartmentSerivce {

	@Autowired
	private DepartmentRepository departmentRepository;

	public Department saveDepartment(Department department) {
	
		log.info("Inside of the saveDepartment method of the DepartmentService");
		return departmentRepository.save(department);
	}


	public Department findDepartmentById(Long departmentId) {
		log.info("inside of the findDepartmentById method of the DepartmentService" );
		return departmentRepository.findByDepartmentId(departmentId);
	}
	

}
